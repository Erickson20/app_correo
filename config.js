module.exports ={
    port: process.env.PORT || 4000,
    host: process.env.HOST || '127.0.0.1',
    dbType: process.env.Type || 'mysql',
    db: process.env.MYSQL || 'AppCorreo',
    userDb: process.env.USER || 'root',
    userPass: process.env.PASS || '',
    SECRET_TOKEN: 'palolazo'
}