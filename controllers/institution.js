'use strict'

const institution = require('../models/institution')





function Create(req, res) {
  
    institution.create({
        nombre: req.body.nombre,
        img: req.body.img
    }).then((result) => {
        res.send({institution: result})
    }).catch((err) => {
        res.send({error: err})
    });
    
}

function AllInstitution(req, res) {
  
    institution.findAll({
        attributes: ['nombre', 'img']
     
    }).then((result) => {
        res.send({institution: result})
    }).catch((err) => {
        res.send({error: err})
    });
    
}

function FindOne(req, res){
    institution.findAll({
        where: {
          id: req.params.id
        }
      }).then((result) => {
            res.send({institution: result})
        }).catch((err) => {
            res.send({error: err})
        });
}


module.exports = {
    Create,
    AllInstitution,
    FindOne
}
