'use strict'

const Emails = require('../models/correo')





function Create(req, res) {
  
    Emails.create({
        nombre: req.body.nombre,
        html: req.body.html,
        institucion: req.body.institucion
    }).then((result) => {
        res.send({email: result})
    }).catch((err) => {
        res.send({error: err})
    });
    
}


function FindAllForID(req, res){
    Emails.findAll({
        where: {
          institucion: req.params.id
        }
      }).then((result) => {
            res.send({email: result})
        }).catch((err) => {
            res.send({error: err})
        });
}

function FindOne(req, res){
    Emails.findAll({
        where: {
          id: req.params.id
        }
      }).then((result) => {
            res.send({email: result})
        }).catch((err) => {
            res.send({error: err})
        });
}


module.exports = {
    Create,
    FindAllForID,
    FindOne
}
