'use strict'
const express = require('express');
const bodyParser = require('body-parser');
const hbs = require('express-handlebars');
const app = express();
const api = require('./routers/index')


app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.engine('.hbs', hbs({
    defaultLayout: 'default',
    extname: '.hbs'
}))
app.set('view engine', '.hbs')

app.use('/api', api)

app.get('/CreateEmail/:id', (req, res) =>{
    res.render('CreateEmail', {
        id: req.params.id
      });
})
app.get('/AllInstitution/:id', (req, res) =>{
    res.render('AllInstitution', {
        id: req.params.id
      });
})
module.exports = app;