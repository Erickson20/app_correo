'use strict'
const db = require('./conect');



const Correo = db.sql.define('correo', {
    id: { type: db.Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
    nombre:{type: db.Sequelize.STRING},
    html: {type: db.Sequelize.TEXT},
    institucion: {type: db.Sequelize.STRING},
    createdAt: {type: db.Sequelize.DATE},
    updatedAt: {type: db.Sequelize.DATE},
  });


module.exports = Correo;