'use strict'
const db = require('./conect');



const Institutions = db.sql.define('instituciones', {
    id: { type: db.Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
    nombre:{type: db.Sequelize.STRING},
    img: {type: db.Sequelize.TEXT},
    createdAt: {type: db.Sequelize.DATE},
    updatedAt: {type: db.Sequelize.DATE},
    
  });


module.exports = Institutions;