
'use strict'
const express = require('express');
const correo = require('../controllers/correo');
const institution = require('../controllers/institution');
//const doc = require('../controllers/certification');
const api = express.Router();



api.post('/Email/', correo.Create);
api.get('/Email/FindAllForID/:id', correo.FindAllForID);
api.get('/Email/:id', correo.FindOne);

api.post('/Institution/', institution.Create);
api.get('/Institution/', institution.AllInstitution);
api.get('/Institution/:id', institution.FindOne);

module.exports = api;